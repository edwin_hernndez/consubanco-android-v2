package com.consubanco;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class SelectDocumentsActivity extends AppCompatActivity {

    CardView cardIne;
    CardView cardCurp;
    CardView cardComp;
    ImageView checkIne;
    ImageView checkCurp;
    ImageView checkComp;
    ImageView imageIne;
    ImageView imageCurp;
    ImageView imageComp;

    Button btnValidar;

    private static final int CAMERA_REQUEST_INE = 1;
    private static final int CAMERA_REQUEST_CURP = 2;
    private static final int CAMERA_REQUEST_COMP = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_documents);

        cardIne = findViewById(R.id.card_ine);
        cardCurp = findViewById(R.id.card_curp);
        cardComp = findViewById(R.id.card_comprobante);

        checkIne = findViewById(R.id.check_ine);
        checkCurp = findViewById(R.id.check_curp);
        checkComp = findViewById(R.id.check_comprobante);

        imageIne = findViewById(R.id.image_ine);
        imageCurp = findViewById(R.id.image_curp);
        imageComp = findViewById(R.id.image_comprobante);

        btnValidar = findViewById(R.id.btn_validar);

        cardIne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_INE);
            }
        });

        cardCurp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_CURP);
            }
        });

        cardComp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_COMP);
            }
        });

        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SelectDocumentsActivity.this, ValidationDataActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case 1:
                if (resultCode == RESULT_OK) {
                    checkIne.setBackgroundResource(R.drawable.ic_check);
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imageIne.setVisibility(View.VISIBLE);
                    imageIne.setImageBitmap(photo);
                }
                break;

            case 2:
                if (resultCode == RESULT_OK) {
                    checkCurp.setBackgroundResource(R.drawable.ic_check);
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imageCurp.setVisibility(View.VISIBLE);
                    imageCurp.setImageBitmap(photo);
                }

                break;

            case 3:
                if (resultCode == RESULT_OK) {
                    checkComp.setBackgroundResource(R.drawable.ic_check);
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imageComp.setVisibility(View.VISIBLE);
                    imageComp.setImageBitmap(photo);
                }
                break;
        }
    }
}