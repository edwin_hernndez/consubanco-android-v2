package com.consubanco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ReadQRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_qr);

        frameLayout = findViewById(R.id.frame_content);
        scannerView = new ZXingScannerView(this);
        frameLayout.addView(scannerView);

    }

    @Override
    public void handleResult(Result rawResult) {
        Log.e("result -->", rawResult.getText());
        Log.e("format -->", rawResult.getBarcodeFormat().toString());

        if (rawResult.getText().length() != 8) {
            Toast.makeText(this, "El código QR es inválido", Toast.LENGTH_SHORT).show();
            scannerView.resumeCameraPreview(this);


        } else if (rawResult.getText().matches("abcdefghijklmnopqrstuvwxyz.,-")) {
            Toast.makeText(this, "El código QR es inválido", Toast.LENGTH_SHORT).show();
            scannerView.resumeCameraPreview(this);

        } else {
            Intent intent = new Intent(ReadQRActivity.this, SelectDocumentsActivity.class);
            startActivity(intent);
            finish();
            scannerView.stopCamera();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }
}
